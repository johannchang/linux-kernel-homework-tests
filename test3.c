#include <stdio.h>
#include <string.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <errno.h>

#define csie_ncu_survey_TT() syscall(__NR_csie_ncu_survey_TT)
#define GREETING "How are you?\n"

int main()
{
	char greeting_array[1000];

	strcpy(greeting_array, GREETING);

	if (csie_ncu_survey_TT()) printf( "Error: %s\n", (char *) strerror(errno));
}
