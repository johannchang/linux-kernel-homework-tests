#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <errno.h>

#define csie_ncu_survey_TT() syscall(__NR_csie_ncu_survey_TT)

int main()
{
	int i, j;
	int array[100][100];

	for (i = 0; i < 100; i++)
		for (j = 0; j < 100; j++)
			array[i][j] = i * j;

	if (csie_ncu_survey_TT()) printf( "Error: %s\n", (char *) strerror(errno));
}
