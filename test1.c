#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <errno.h>

#define csie_ncu_survey_TT() syscall(__NR_csie_ncu_survey_TT)

int main()
{
	printf("Hello World!\n");
	if (csie_ncu_survey_TT()) printf( "Error: %s\n", (char *) strerror(errno));
}
